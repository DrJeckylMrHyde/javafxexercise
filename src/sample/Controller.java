package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.util.Optional;

//klasa służąca do obsługi zawartości okna
public class Controller {

    @FXML
    private Label name_label;
    @FXML
    private Label age_label;
    @FXML
    private Label phone_label;
    @FXML
    private TableView<Person> people_list;
    @FXML
    private TableColumn<Person, String> name_column;
    @FXML
    private TableColumn<Person, Number> age_column;
    @FXML
    private Button delete_button;

    private Main main;

    public void setMain(Main main) {
        this.main = main;

        people_list.setItems(main.getPersonObservableList());

    }

    @FXML
    private void initialize() {
        name_column.setCellValueFactory(cell -> cell.getValue().nameProperty());
        age_column.setCellValueFactory(cell -> cell.getValue().ageProperty());

        people_list.getSelectionModel()
                .selectedItemProperty()
                .addListener(new ChangeListener<Person>() {
                    @Override
                    public void changed(ObservableValue<? extends Person> observable, Person oldValue, Person newValue) {
                        showPersonDetails(newValue);
                    }
                });
    }

    private void showPersonDetails(Person person) {
        if (person == null) {

            name_label.setText("");
            age_label.setText("");
            phone_label.setText("");
        } else {
            name_label.setText(person.getName());
            age_label.setText(String.valueOf(person.getAge()));
            phone_label.setText(person.getPhone());
        }
    }

    @FXML
    private void handleDeletePerson() {
        int selectedPosition = people_list.getSelectionModel().getSelectedIndex();

        if (selectedPosition == -1) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("No selection");
            alert.setContentText("Please select person from the table");
            alert.showAndWait();
        } else {

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Removing");

            String name = people_list.getItems().get(selectedPosition).getName();

            alert.setContentText("Are you sure to remove: "+name);

            Optional<ButtonType> buttonType = alert.showAndWait();

            if(buttonType.get() == ButtonType.OK){
            people_list.getItems().remove(selectedPosition);
            } else if (buttonType.get() == ButtonType.CANCEL){

            }
        }
    }


}
