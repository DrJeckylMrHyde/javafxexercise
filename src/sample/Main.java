package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.Observable;

public class Main extends Application {

//    lista osób
    private static ObservableList<Person> personObservableList =
            FXCollections.observableArrayList();

    @Override
    public void start(Stage primaryStage) throws Exception{

//      Tworzymy obiekt umożliwiający połaczenie logiki z wyglądem
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("sample.fxml"));

        AnchorPane anchorPane = loader.load();

        Controller controller = loader.getController();
        controller.setMain(this);

        Scene scene = new Scene(anchorPane);

        primaryStage.setTitle("Hello World");

//        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setScene(scene);


//        Wyłączenie zmiany rozmiaru okna
        primaryStage.setResizable(false);

        primaryStage.show();
    }


    public static void main(String[] args) {
        fillList();
        launch(args);
    }

    private static void fillList() {
        personObservableList.addAll(Arrays.asList(
                new Person("Jan",20,"123-567-098"),
                new Person("Kuba",11,"123-123-123"),
                new Person("Natalia",22,"123-123-123"),
                new Person("Monika",18,"865534345"),
                new Person("Przemek",11,"111222333")
        ));
    }

    public static ObservableList<Person> getPersonObservableList() {
        return personObservableList;
    }
}
